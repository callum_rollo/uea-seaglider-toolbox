function data = gt_sg_flightmodelregression(data,opt)
%
% Inputs:
% data = standard data structure from toolbox
% opt = options
%   opt always causes processing to happen non-interactively.
%   opt can be:
%     -1: Does not regress, only calculates flight parameters.
%     0: Auto regress on all valid dives.
%     numeric array: Selection of dives to auto-regress.
%     'timevar': Calculates time-varying hd_a, b, s and volmax parameters
%           in 30 dive chunks.
%     
% If hd_* parameters are 1 x numel(dives) size, each dive will be
% calculated with its own hd_* parameters and the GUI bypassed.
%
% data = gt_sg_sub_flightvec(data)
%
% Wrapper for the UW flightvec.m routines which calculates, using the
% hydrodynamic flight model and buoyancy, the speed and glide angle of the
% glider. This is a better (and iterative) approximation of flight than the
% glider_slope version.
%
% All credits to original authors at the University of Washington
%
% TODO: clear gt_sg_settings.regress_history when running a timevar in case
% a 1-D version already exists. 
%
% B.Y.QUESTE Nov 2018
%
HelpInfo = ...
    {'If you want the quick and easy option, just click the button on the right. ->',...
    ' ',...
    'Otherwise, use the first tab to select useful data and regress in the second tab. The two cost functions work better on different things. The one labelled hydro works best on hd_ parameters. The one labelled buoyancy copes best with volmax, abs_compress and therm_expan.',...
    ' ',...
    'Info on visualisation tab...'};

%% Setup settings storage
dives = gt_sg_sub_check_dive_validity(data);

% Data selection
FRS.surface_cutoff = 10;
FRS.apogee_cutoff = 10;
FRS.roll_max = 5;
FRS.pitch_max = 90;
FRS.stall_max = 11;
FRS.motor_trim = 2;
FRS.dives = dives;
FRS.use_regress = [];
FRS.vertaccel_max = 0.002;
FRS.vertvel_min = 0.02;

if isfield(data.gt_sg_settings,'flight_regression_settings')
    fields = fieldnames(data.gt_sg_settings.flight_regression_settings);
    for fstep = 1:numel(fields)
        FRS.(fields{fstep}) = data.gt_sg_settings.flight_regression_settings.(fields{fstep});
    end
end

%% Extract basic variables
cast_dir = arrayfun(@(x) ((x.elaps_t*0)+1).*x.dive,data.eng,'Uni',0); % Dive no
for dstep=1:numel(cast_dir)
    cast_dir{dstep}(1:data.hydrography(dstep).max_pressure_index) = -cast_dir{dstep}(1:data.hydrography(dstep).max_pressure_index); % Sign for direction
end

if isfield(data,'flag')
    good_ind = arrayfun(@(x) ~x.salinity & ~x.conductivity & ~x.temp,data.flag,'Uni',0);
else
    good_ind = arrayfun(@(x) true(size(x.time)),data.hydrography,'Uni',0);
end

% Hull length
HullLength = {'Standard',1.8;...
    'Ogive',2.0;...
    'Deep (N/A)',2.2};
if isfield(data.gt_sg_settings,'length')
    hullLength = data.gt_sg_settings.length;
else
    try
        hullLength = mode([data.log.LENGTH]);
    catch
        hullLength = 1.8;
    end
end
hullLength = find([HullLength{:,2}] == hullLength);
data.gt_sg_settings.length = HullLength{hullLength,2};

if ~isfield(data.gt_sg_settings,'hd_s')
    data.gt_sg_settings.hd_s = -1/4;
end

% Set default constraint limits
h_regress_para.hd_a.min = 0.001;    h_regress_para.hd_a.max = 0.007;
h_regress_para.hd_b.min = 0.004;    h_regress_para.hd_b.max = 0.02;
h_regress_para.hd_c.min = 1.0e-6;   h_regress_para.hd_c.max = 3.0e-5;
h_regress_para.hd_s.min = -1;       h_regress_para.hd_s.max = 0;
h_regress_para.volmax.min = 5.0e4;  h_regress_para.volmax.max = 5.5e4;
h_regress_para.abs_compress.min = 1.0e-06; h_regress_para.abs_compress.max = 3.0e-5;
h_regress_para.therm_expan.min = 5.0e-05; h_regress_para.therm_expan.max = 5.0e-4;

%% Create panel
close all;
timevar = false;
if nargin > 1
    h_gui = figure('Visible','off');
    
    if isnumeric(opt)
        diveSubset = opt;
    elseif strcmpi(opt,'timevar')
        timevar = true;
        diveSubset = 0;
    else
        gt_sg_sub_echo({'Invalid flight model gui input "opt" parameter.'});
        return;
    end
    
    if diveSubset == -1
        FRS.dives = gt_sg_sub_check_dive_validity(data);
        gt_sg_sub_echo({'Applying hydrodynamic calculations to all dives.'});
        run_flightmodel(data.gt_sg_settings);
        return;
    elseif diveSubset == 0
        diveSubset = dives;
    end
    
    gt_sg_sub_echo({'Running automated flight model regression on dives:',num2str(diveSubset)});
    auto = true;
    FRS.dives = gt_sg_sub_check_dive_validity(data,diveSubset);
else
    h_gui = figure('Visible','off');
    % Set options
    set(h_gui,...
        'MenuBar','none',...
        'Units','normalized',...
        'Position',[0.05 0.05 0.9 0.9]...
        );
    auto = false;
end

%% Populate tabs
h_tabs = uitabgroup(h_gui,'Position',[0 0 1 1]);
h_tab(1) = uitab(h_tabs,'Title','1. Data Selection');
h_tab(2) = uitab(h_tabs,'Title','2. Parameter Regression');
h_tab(3) = uitab(h_tabs,'Title','3. Visualisation');
h_tab(4) = uitab(h_tabs,'Title','4. History');
h_tab(5) = uitab(h_tabs,'Title','5. Help');

%% Tab 1 - Data Selection
uicontrol(h_tab(1),'Style','pushbutton','String','Save settings',...
    'Units','normalized','Position',[0.87 0.9 0.12 0.07],...
    'CallBack',@button_data_save);
uicontrol(h_tab(1),'Style','pushbutton','String','Plot selection',...
    'Units','normalized','Position',[0.74 0.9 0.12 0.07],...
    'CallBack',@button_data_plot);

% Apogee and surface cutoff
uicontrol(h_tab(1),'Style','text','String','Surface cutoff (m)',...
    'Units','normalized','Position',[0.01 0.94 0.10 0.03]);
uicontrol(h_tab(1),'Style','text','String','Apogee cutoff (m)',...
    'Units','normalized','Position',[0.01 0.90 0.10 0.03]);
h_data_cutoff_surface = uicontrol(h_tab(1),'Style','edit','String',FRS.surface_cutoff,...
    'Units','normalized','Position',[0.12 0.94 0.06 0.03]);
h_data_cutoff_apogee = uicontrol(h_tab(1),'Style','edit','String',FRS.apogee_cutoff,...
    'Units','normalized','Position',[0.12 0.90 0.06 0.03]);

% Roll, pitch, stall and post motor movement cutoff
uicontrol(h_tab(1),'Style','text','String','Roll max (deg)',...
    'Units','normalized','Position',[0.19 0.94 0.10 0.03]);
uicontrol(h_tab(1),'Style','text','String','Pitch max (deg)',...
    'Units','normalized','Position',[0.19 0.90 0.10 0.03]);
h_data_cutoff_rollmax = uicontrol(h_tab(1),'Style','edit','String',FRS.roll_max,...
    'Units','normalized','Position',[0.30 0.94 0.06 0.03]);
h_data_cutoff_pitchmax = uicontrol(h_tab(1),'Style','edit','String',FRS.pitch_max,...
    'Units','normalized','Position',[0.30 0.90 0.06 0.03]);
uicontrol(h_tab(1),'Style','text','String','Stall angle (deg)',...
    'Units','normalized','Position',[0.37 0.94 0.10 0.03]);
uicontrol(h_tab(1),'Style','text','String','Motor trim (samples)',...
    'Units','normalized','Position',[0.37 0.90 0.10 0.03]);
h_data_cutoff_stallmax = uicontrol(h_tab(1),'Style','edit','String',FRS.stall_max,...
    'Units','normalized','Position',[0.48 0.94 0.06 0.03]);
h_data_cutoff_motor = uicontrol(h_tab(1),'Style','edit','String',FRS.motor_trim,...
    'Units','normalized','Position',[0.48 0.90 0.06 0.03]);
uicontrol(h_tab(1),'Style','text','String','Min dP/dt (m/s)',...
    'Units','normalized','Position',[0.55 0.94 0.10 0.03]);
uicontrol(h_tab(1),'Style','text','String','Max accel. (m/s^2)',...
    'Units','normalized','Position',[0.55 0.90 0.10 0.03]);
h_data_cutoff_vertvelmin = uicontrol(h_tab(1),'Style','edit','String',FRS.vertvel_min,...
    'Units','normalized','Position',[0.66 0.94 0.06 0.03]);
h_data_cutoff_vertaccelmax = uicontrol(h_tab(1),'Style','edit','String',FRS.vertaccel_max,...
    'Units','normalized','Position',[0.66 0.90 0.06 0.03]);

% Dive Selection
uicontrol(h_tab(1),'Style','text','String','Dive numbers',...
    'Units','normalized','Position',[0.01 0.86 0.10 0.03]);
h_data_list_dives = uicontrol(h_tab(1),'Style','listbox','String',dives,...
    'Units','normalized','Position',[0.12 0.01 0.06 0.88],...
    'Max',10000,'Min',1,'Value',indicesIn(dives,FRS.dives));
uicontrol(h_tab(1),'Style','pushbutton','String','Select all',...
    'Units','normalized','Position',[0.01 0.05 0.10 0.03],...
    'CallBack',@(x,y) set(h_data_list_dives,'Value',1:numel(dives)));
uicontrol(h_tab(1),'Style','pushbutton','String','Select none',...
    'Units','normalized','Position',[0.01 0.01 0.10 0.03],...
    'CallBack',@(x,y) set(h_data_list_dives,'Value',[]));

% Plots
h_data_plot_pitch = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.2 0.035 0.74 0.14],'Parent',h_tab(1));
set(get(h_data_plot_pitch,'YLabel'),'String','Pitch angle (deg)')
hold on;
plot(h_data_plot_pitch,[data.hydrography(dives).time],[data.eng(dives).pitchAng],'LineStyle','none','Marker','.','MarkerSize',1,'Color',[0.8 0.8 0.8]);
h_data_plot_pitch_blue = plot(h_data_plot_pitch,[],[],'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_pitch,'x'); axis(h_data_plot_pitch,'tight');

h_data_plot_roll = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.2 0.21 0.74 0.14],'Parent',h_tab(1));
set(get(h_data_plot_roll,'YLabel'),'String','Roll angle (deg)')
hold on;
plot(h_data_plot_roll,[data.hydrography(dives).time],[data.eng(dives).rollAng],'LineStyle','none','Marker','.','MarkerSize',1,'Color',[0.8 0.8 0.8]);
h_data_plot_roll_blue = plot(h_data_plot_roll,[],[],'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_roll,'x'); axis(h_data_plot_roll,'tight');

h_data_plot_total = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.2 0.385 0.74 0.14],'Parent',h_tab(1));
set(get(h_data_plot_total,'YLabel'),'String','dP/dt (m/s)')
hold on;
plot(h_data_plot_total,[data.hydrography(dives).time],[data.flight(dives).glide_vert_spd],'LineStyle','none','Marker','.','MarkerSize',1,'Color',[0.8 0.8 0.8]);
h_data_plot_total_blue = plot(h_data_plot_total,[],[],'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_total,'x'); axis(h_data_plot_total,'tight');

h_data_plot_accel = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.2 0.56 0.74 0.14],'Parent',h_tab(1));
set(get(h_data_plot_accel,'YLabel'),'String','Acceleration (m/s^2)')
hold on;
plot(h_data_plot_accel,[data.hydrography(dives).time],[data.flight(dives).glide_vert_spd],'LineStyle','none','Marker','.','MarkerSize',1,'Color',[0.8 0.8 0.8]);
h_data_plot_accel_blue = plot(h_data_plot_total,[],[],'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_accel,'x'); axis(h_data_plot_accel,'tight');

h_data_plot_vbd = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.2 0.735 0.74 0.14],'Parent',h_tab(1));
set(get(h_data_plot_vbd,'YLabel'),'String','C\_VBD')
hold on;
plot(h_data_plot_vbd,dives,[data.log(dives).C_VBD],'LineStyle','none','Marker','*','Color',[0.8 0.8 0.8]);
h_data_plot_vbd_blue = plot(h_data_plot_vbd,[],[],'LineStyle','none','Marker','*','Color','b'); axis(h_data_plot_vbd,'tight');

% Populate with current selection
func_data_makeplots;

% Callbacks
    function button_data_save(button_data,event_data)
        button_data_plot(button_data,event_data)
        data.gt_sg_settings.flight_regression_settings = FRS;
        set(h_tabs,'SelectedTab',h_tab(2));
        
        % Initialise second tab
        button_regress_regresscheckbox(0,0);
        button_regress_plot_velocities(0,0);
    end

    function button_data_plot(~,~)
        FRS.surface_cutoff = str2double(get(h_data_cutoff_surface,'String'));
        FRS.apogee_cutoff = str2double(get(h_data_cutoff_apogee,'String'));
        FRS.roll_max = str2double(get(h_data_cutoff_rollmax ,'String'));
        FRS.pitch_max = str2double(get(h_data_cutoff_pitchmax,'String'));
        FRS.stall_max = str2double(get(h_data_cutoff_stallmax,'String'));
        FRS.motor_trim = str2double(get(h_data_cutoff_motor,'String'));
        FRS.vertvel_min = str2double(get(h_data_cutoff_vertvelmin,'String'));
        FRS.vertaccel_max = str2double(get(h_data_cutoff_vertaccelmax,'String'));
        FRS.dives = dives(get(h_data_list_dives,'Value'));
        
        func_data_makeplots;
    end

    function func_data_makeplots
        delete(h_data_plot_total_blue);
        delete(h_data_plot_accel_blue);
        delete(h_data_plot_vbd_blue);
        delete(h_data_plot_pitch_blue);
        delete(h_data_plot_roll_blue);
        
        for dstep = 1:numel(data.hydrography);
            FRS.use_regress{dstep} = false(size(data.hydrography(dstep).depth));
            if any(FRS.dives == dstep)
                GC_phase = data.eng(dstep).GC_phase == 6;
                if FRS.motor_trim < 1
                    GC_phase(:) = true;
                elseif FRS.motor_trim > 1;
                    GC_trim = @(gc) [gc - [0 diff(gc)> 0]];
                    for istep = 1:ceil(FRS.motor_trim)-1
                        GC_phase = GC_trim(GC_phase);
                    end
                end
                FRS.use_regress{dstep}(...
                    data.hydrography(dstep).depth > FRS.surface_cutoff ...
                    & data.hydrography(dstep).depth < data.hydrography(dstep).depth(data.hydrography(dstep).max_pressure_index)-FRS.apogee_cutoff ...
                    & abs(data.eng(dstep).rollAng) < FRS.roll_max ...
                    & abs(data.eng(dstep).pitchAng) < FRS.pitch_max ...
                    & abs(data.eng(dstep).pitchAng) > FRS.stall_max ...
                    & abs(data.flight(dstep).glide_vert_spd) > FRS.vertvel_min ...
                    & abs(gt_sg_sub_diff({data.flight(dstep).glide_vert_spd,data.hydrography(dstep).time.*24.*60.*60})) < FRS.vertaccel_max ...
                    & GC_phase ...
                    & good_ind{dstep} ...
                    ) = true;
            end
        end
        
        ind = [FRS.use_regress{dives}];
        time = [data.hydrography(dives).time];
        vert_spd = [data.flight(dives).glide_vert_spd];
        pitchAng = [data.eng(dives).pitchAng];
        rollAng = [data.eng(dives).rollAng];
        accel = gt_sg_sub_diff({[data.flight.glide_vert_spd],[data.hydrography.time]*24*60*60});
        
        uicontrol(h_tab(1),'Style','text','String',{'Number of Points:',num2str(sum(ind)),'of',num2str(numel(time))},...
            'Units','normalized','Position',[0.01 0.4 0.10 0.1],...
            'CallBack',@(x,y) set(h_data_list_dives,'Value',1:numel(dives)));
        
        h_data_plot_pitch_blue = plot(h_data_plot_pitch,time(ind),pitchAng(ind),'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_pitch,'x'); axis(h_data_plot_pitch,'tight');
        h_data_plot_roll_blue = plot(h_data_plot_roll,time(ind),rollAng(ind),'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_roll,'x'); axis(h_data_plot_roll,'tight');
        h_data_plot_total_blue = plot(h_data_plot_total,time(ind),vert_spd(ind),'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_total,'x'); axis(h_data_plot_total,'tight');
        h_data_plot_accel_blue = plot(h_data_plot_accel,time(ind),accel(ind),'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_total,'x'); axis(h_data_plot_total,'tight');
        h_data_plot_vbd_blue = plot(h_data_plot_vbd,FRS.dives,[data.log(FRS.dives).C_VBD],'LineStyle','none','Marker','*','Color','b'); axis(h_data_plot_vbd,'tight');
    end

%% Tab 2 - Parameter Regression
uicontrol(h_tab(2),'Style','pushbutton','String','View results',...
    'Units','normalized','Position',[0.87 0.91 0.12 0.07],...
    'CallBack',@button_regress_plot_velocities);
uicontrol(h_tab(2),'Style','pushbutton','String','Store results',...
    'Units','normalized','Position',[0.87 0.83 0.12 0.07],...
    'CallBack',@button_regress_store);
uicontrol(h_tab(2),'Style','pushbutton','String','Undo',...
    'Units','normalized','Position',[0.87 0.75 0.12 0.07],...
    'CallBack',@button_regress_undo);
uicontrol(h_tab(2),'Style','pushbutton','String','Restore defaults',...
    'Units','normalized','Position',[0.87 0.67 0.12 0.07],...
    'CallBack',@button_regress_reset);

% Panels
h_regress_buoy = uipanel('Parent',h_tab(2),'Title','Buoyancy parameters',...
    'Units','normalized','Position',[0.01 0.01 0.485 0.41]);
h_regress_hydro = uipanel('Parent',h_tab(2),'Title','Hydrodynamic parameters',...
    'Units','normalized','Position',[0.01 0.43 0.485 0.55]);
h_regress_cost = uibuttongroup('Parent',h_tab(2),'Title','Cost function',...
    'Units','normalized','Position',[0.50 0.505 0.18 .485]);
h_regress_opt = uipanel('Parent',h_tab(2),'Title','Settings',...
    'Units','normalized','Position',[0.685 0.505 0.18 .485]);
h_regress_iter = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.50 0.1 0.21 0.39],'Parent',h_tab(2)); hold on;
h_regress_vis = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.76 0.1 0.21 0.39],'Parent',h_tab(2)); ...
    hold on; ylabel(h_regress_vis,'Depth (m)'); xlabel(h_regress_vis,{'Red - dP/dt, Blue - buoyancy,','Green - up/downwelling (m.s^-^1)'});

% Settings
uicontrol(h_regress_opt,'Style','text','String','Fairing type:',...
    'Units','normalized','Position',[0.01 0.93 0.48 0.04]);
h_regress_opt_length = uicontrol(h_regress_opt,'Style','popup','String',HullLength(:,1),'Value',hullLength,...
    'Min',1,'Max',1,'Units','normalized','Position',[0.51 0.93 0.48 0.04],...
    'CallBack',...
    @button_regress_hullLength);
uicontrol(h_regress_opt,'Style','text','String','Constrain:',...
    'Units','normalized','Position',[0.01 0.82 0.48 0.04]); constrainRegression = {'Off','On'};
h_regress_opt_constrain = uicontrol(h_regress_opt,'Style','popup','String',constrainRegression,'Value',1,...
    'Min',1,'Max',1,'Units','normalized','Position',[0.51 0.82 0.48 0.04],...
    'CallBack',@button_regress_constrain);
uicontrol(h_regress_opt,'Style','text','String','Max iterations: ',...
    'Units','normalized','Position',[0.01 0.71 0.48 0.04]);
h_regress_opt_maxiter = uicontrol(h_regress_opt,'Style','edit','String',100,...
    'Units','normalized','Position',[0.50 0.68 0.48 0.1]);
uicontrol(h_regress_opt,'Style','text','String','Function tolerance:',...
    'Units','normalized','Position',[0.01 0.60 0.48 0.04]);
h_regress_opt_tolfun = uicontrol(h_regress_opt,'Style','edit','String',1.0e-10,...
    'Units','normalized','Position',[0.50 0.57 0.48 0.1]);
h_regress_button_process = uicontrol(h_regress_opt,'Style','pushbutton','String','Regress parameters',...
    'Units','normalized','Position',[0.17 0.41 0.74 0.14],...
    'CallBack',@button_regress_process);
h_regress_button_auto = uicontrol(h_regress_opt,'Style','pushbutton','String','Attempt auto regress',...
    'Units','normalized','Position',[0.17 0.13 0.74 0.14],...
    'CallBack',@button_regress_auto,'Enable','on');
uicontrol(h_regress_opt,'Style','text','String',{'Number of','rangefinding passes:'},...
    'Units','normalized','Position',[0.01 0.01 0.48 0.1]);
h_regress_opt_autopass = uicontrol(h_regress_opt,'Style','edit','String','1',...
    'Units','normalized','Position',[0.50 0.01 0.48 0.1],'Enable','on');

% Cost function options
h_regress_cost_radio(1) = uicontrol(h_regress_cost,'Style','radiobutton','String',...
    'Minimise mean(W(H2O)) (buoyancy)',...
    'Units','normalized','Position',[0.01 0.9 0.98 0.1]);
h_regress_cost_radio(2) = uicontrol(h_regress_cost,'Style','radiobutton','String',...
    'Minimise mode(W(H2O)) (convective environments)',...
    'Units','normalized','Position',[0.01 0.7 0.98 0.1]);
h_regress_cost_radio(3) = uicontrol(h_regress_cost,'Style','radiobutton','String',...
    'RMSD of up & down W(H2O) (hydro)',...
    'Units','normalized','Position',[0.01 0.5 0.98 0.1],'Enable','on');
h_regress_cost_radio(4) = uicontrol(h_regress_cost,'Style','radiobutton','String',...
    'dP/dt vs. pitch space minimisation',...
    'Units','normalized','Position',[0.01 0.3 0.98 0.1],'Enable','on');
% h_regress_cost_radio(5) = uicontrol(h_regress_cost,'Style','radiobutton','String',...
%     'Cost fun 1 * cost fun 2 ?? Something nice and overarching?',...
%     'Units','normalized','Position',[0.01 0.1 0.98 0.1],'Enable','off');

% Hydro and buoy panels
h_regress_para.hd_a.panel = uipanel('Parent',h_regress_hydro,'Title','HD_A: "Lift"',...
    'Units','normalized','Position',[0.01 0.05+3*(1-0.08)/4 0.98 (1-0.08)/4]);
h_regress_para.hd_b.panel = uipanel('Parent',h_regress_hydro,'Title','HD_B: Drag',...
    'Units','normalized','Position',[0.01 0.04+2*(1-0.08)/4 0.98 (1-0.08)/4]);
h_regress_para.hd_c.panel = uipanel('Parent',h_regress_hydro,'Title','HD_C: Induced drag',...
    'Units','normalized','Position',[0.01 0.03+(1-0.08)/4 0.98 (1-0.08)/4]);
h_regress_para.hd_s.panel = uipanel('Parent',h_regress_hydro,'Title','HD_S: Magic',...
    'Units','normalized','Position',[0.01 0.02 0.98 (1-0.08)/4]);
h_regress_para.volmax.panel = uipanel('Parent',h_regress_buoy,'Title','Volmax',...
    'Units','normalized','Position',[0.01 0.04+2*(1-0.06)/3 0.98 (1-0.06)/3]);
h_regress_para.abs_compress.panel = uipanel('Parent',h_regress_buoy,'Title','Absolute compression',...
    'Units','normalized','Position',[0.01 0.03+(1-0.06)/3 0.98 (1-0.06)/3]);
h_regress_para.therm_expan.panel = uipanel('Parent',h_regress_buoy,'Title','Thermal expansion',...
    'Units','normalized','Position',[0.01 0.02 0.98 (1-0.06)/3]);

% Populate parameter panels
parameters = {'hd_a','hd_b','hd_c','hd_s','volmax','abs_compress','therm_expan'};
for pstep = 1:numel(parameters)
    
    h_regress_para.(parameters{pstep}).regress = uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','checkbox','String','Regress?',...
        'Units','normalized','Position',[0.01 0.8 0.28 0.2],...
        'CallBack',@button_regress_regresscheckbox);
    
    h_regress_para.(parameters{pstep}).initial = uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','edit','String',data.gt_sg_settings.(parameters{pstep}),...
        'Enable',constrainRegression{get(h_regress_opt_constrain,'Value')}, 'Units','normalized','Position',[0.51 0.8 0.48 0.2]);
    uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','text','String','Initial guess: ',...
        'Units','normalized','Position',[0.30 0.8 0.2 0.2]);
    
    h_regress_para.(parameters{pstep}).minval = uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','edit','String',h_regress_para.(parameters{pstep}).min,...
        'Enable',constrainRegression{get(h_regress_opt_constrain,'Value')}, 'Units','normalized','Position',[0.41 0.05 0.23 0.2]);
    uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','text','String','Minimum: ',...
        'Units','normalized','Position',[0.30 0.05 0.11 0.2]);
    
    h_regress_para.(parameters{pstep}).maxval = uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','edit','String',h_regress_para.(parameters{pstep}).max,...
        'Enable',constrainRegression{get(h_regress_opt_constrain,'Value')}, 'Units','normalized','Position',[0.76 0.05 0.23 0.2]);
    uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','text','String','Maximum: ',...
        'Units','normalized','Position',[0.65 0.05 0.11 0.2]);
    
    undo.(parameters{pstep}) = data.gt_sg_settings.(parameters{pstep});
end

% Callbacks
    function button_regress_process(~,~)
        stop_regression = false;
        set(h_regress_button_process,'String','Stop regression',...
            'CallBack',@button_regress_stop); drawnow;
        
        for pstep = 1:numel(parameters)
            undo.(parameters{pstep}) = data.gt_sg_settings.(parameters{pstep});
            settings.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).initial,'String'));
            param_min.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).minval,'String'));
            param_max.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).maxval,'String'));
        end
        % 1) make all inputs same order of magnitude
        param_coeffs = get_coeff([settings.hd_a,settings.hd_b,settings.hd_c,settings.hd_s,settings.volmax,settings.abs_compress,settings.therm_expan]);
        param_exp = get_exp([settings.hd_a,settings.hd_b,settings.hd_c,settings.hd_s,settings.volmax,settings.abs_compress,settings.therm_expan]);
        clear settings % IMPORTANT SO AS NOT TO CROSS CONTAMINATE REGRESSION SUBFUNCTION
        % 2) set options
        options = optimset('Display','iter',...
            'OutputFcn',@button_regress_plot_iteration,...
            'MaxIter',str2double(get(h_regress_opt_maxiter,'String')),...
            'TolFun',str2double(get(h_regress_opt_tolfun,'String')));
        % 3) initiate iteration plot
        delete(get(h_regress_iter,'Children'))
        set(h_regress_iter,...
            'XLim',[0 str2double(get(h_regress_opt_maxiter,'String'))],...
            'XLimMode','manual');
        h_regress_iter_plot = plot(h_regress_iter,...
            [0:str2double(get(h_regress_opt_maxiter,'String'))],...
            nan(str2double(get(h_regress_opt_maxiter,'String'))+1,1),...
            'LineStyle','none','Marker','d');
        % 4) select appropriate cost function
        switch find(cellfun(@(x) x==1, get(h_regress_cost_radio,'Value')))
            case 1
                h_regress_scorefun = @button_regress_score_minmeanwh2o;
            case 2
                h_regress_scorefun = @button_regress_score_minmodewh2o;
            case 3
                h_regress_scorefun = @button_regress_score_minuddiff;
            case 4
                h_regress_scorefun = @button_regress_score_pitchbuoyspace;
            case 5
                %h_regress_scorefun = @
        end
        % 5) do regression AND reapply exponents
        if strcmpi(constrainRegression{get(h_regress_opt_constrain,'Value')},'on')
            for pstep = 1:numel(parameters)
                param_min.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).minval,'String'));
                param_max.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).maxval,'String'));
            end
            param_min = [param_min.hd_a,param_min.hd_b,param_min.hd_c,param_min.hd_s,param_min.volmax,param_min.abs_compress,param_min.therm_expan] ./ (10.^param_exp);
            param_max = [param_max.hd_a,param_max.hd_b,param_max.hd_c,param_max.hd_s,param_max.volmax,param_max.abs_compress,param_max.therm_expan] ./ (10.^param_exp);
            param_regressed = real(fmincon(@regress_parameters,param_coeffs,[],[],[],[],param_min,param_max,[],options) .* (10.^param_exp));
        else
            param_regressed = real(fminsearch(@regress_parameters,param_coeffs,options) .* (10.^param_exp));
        end
        % 6) joyfully announce it to the world by replacing settings and
        % variables in GUI.
        
        for pstep = 1:numel(parameters)
            if get(h_regress_para.(parameters{pstep}).regress,'Value') == 1
                data.gt_sg_settings.(parameters{pstep}) = param_regressed(pstep);
            end
            set(h_regress_para.(parameters{pstep}).initial,'String',data.gt_sg_settings.(parameters{pstep}));
        end
        
        button_regress_displaydefaults
        button_regress_plot_velocities(0,0)
        
        data.gt_sg_settings.regress_history(end+1,:) = {datestr(now),data.gt_sg_settings.hd_a,data.gt_sg_settings.hd_b,data.gt_sg_settings.hd_c,data.gt_sg_settings.hd_s,data.gt_sg_settings.volmax,data.gt_sg_settings.abs_compress,data.gt_sg_settings.therm_expan,' '};
        set(h_his_tables,'Data',data.gt_sg_settings.regress_history);
        
        set(h_regress_button_process,'String','Regress parameters',...
            'CallBack',@button_regress_process); drawnow;
        
        function score = regress_parameters(param)
            % Define settings
            params = param .* (10.^param_exp);
            settings.hd_a = params(1);
            settings.hd_b = params(2);
            settings.hd_c = params(3);
            settings.hd_s = params(4);
            settings.volmax = params(5);
            settings.abs_compress = params(6);
            settings.therm_expan = params(7);
            settings.length = data.gt_sg_settings.length;
            % Reset variables we don't want to regress
            for pstep = 1:numel(parameters)
                if get(h_regress_para.(parameters{pstep}).regress,'Value') == 0
                    settings.(parameters{pstep}) = data.gt_sg_settings.(parameters{pstep});
                end
            end
            % Run the model
            run_flightmodel(settings)
            
            % SCORE USING THE INDEX TO REMOVE BAD DATA
            score = h_regress_scorefun();
        end
        
        function stop = button_regress_plot_iteration(~,optimValues,~)
            stop = stop_regression;
            YData = get(h_regress_iter_plot,'YData');
            YData(get(h_regress_iter_plot,'XData') == optimValues.iteration) = optimValues.fval;
            set(h_regress_iter_plot,'YData',YData);
            drawnow;
        end
        
        function button_regress_stop(~,~)
            stop_regression = true;
        end
        
    end

    function button_regress_auto(~,~)
        for istep = 1:str2double(get(h_regress_opt_autopass,'String'))
            set(h_regress_cost_radio(1),'Value',1);
            set(h_regress_para.hd_a.regress,'Value',1);
            set(h_regress_para.hd_b.regress,'Value',1);
            set(h_regress_para.hd_c.regress,'Value',0);
            set(h_regress_para.hd_s.regress,'Value',1);
            set(h_regress_para.volmax.regress,'Value',1);
            set(h_regress_para.abs_compress.regress,'Value',0);
            set(h_regress_para.therm_expan.regress,'Value',0);
            drawnow;
            button_regress_process; drawnow;
        end
    end

    function button_regress_store(~,~)
        for pstep = 1:numel(parameters)
            undo.(parameters{pstep}) = data.gt_sg_settings.(parameters{pstep});
            data.gt_sg_settings.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).initial,'String'));
        end
        button_regress_displaydefaults
    end

    function button_regress_undo(~,~)
        for pstep = 1:numel(parameters)
            set(h_regress_para.(parameters{pstep}).initial,'String',undo.(parameters{pstep}));
        end
        button_regress_store(0,0)
    end

    function button_regress_reset(~,~)
        for pstep = 1:numel(parameters)
            undo.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).initial,'String'));
            set(h_regress_para.(parameters{pstep}).initial,'String',data.gt_sg_settings.(parameters{pstep}));
            set(h_regress_para.(parameters{pstep}).maxval,'String',h_regress_para.(parameters{pstep}).max);
            set(h_regress_para.(parameters{pstep}).minval,'String',h_regress_para.(parameters{pstep}).min);
        end
    end

    function button_regress_regresscheckbox(~,~)
        for fstep = 1:numel(parameters)
            if get(h_regress_para.(parameters{fstep}).regress,'Value') == 1
                set(h_regress_para.(parameters{fstep}).initial,'Enable','on');
                button_regress_constrain(0,0);
            else
                set(h_regress_para.(parameters{fstep}).initial,'Enable','off');
                set(h_regress_para.(parameters{fstep}).maxval,'Enable','off');
                set(h_regress_para.(parameters{fstep}).minval,'Enable','off');
            end
        end
        
        button_regress_displaydefaults;
    end

    function button_regress_hullLength(~,~)
        data.gt_sg_settings.length = HullLength{get(h_regress_opt_length,'Value'),2};
    end

    function button_regress_constrain(~,~)
        for fstep = 1:numel(parameters)
            if get(h_regress_para.(parameters{fstep}).regress,'Value') == 1
                set(h_regress_para.(parameters{fstep}).maxval,'Enable',constrainRegression{get(h_regress_opt_constrain,'Value')});
                set(h_regress_para.(parameters{fstep}).minval,'Enable',constrainRegression{get(h_regress_opt_constrain,'Value')});
            end
        end
    end

    function button_regress_displaydefaults
        uicontrol(h_tab(2),'Style','text','String',...
            {['HD_A: ',num2str(data.gt_sg_settings.hd_a)],...
            ['HD_B: ',num2str(data.gt_sg_settings.hd_b)],...
            ['HD_C: ',num2str(data.gt_sg_settings.hd_c)],...
            ['HD_S: ',num2str(data.gt_sg_settings.hd_s)],...
            ['V_MAX: ',num2str(data.gt_sg_settings.volmax)],...
            ['A_CMP: ',num2str(data.gt_sg_settings.abs_compress)],...
            ['T_EXP: ',num2str(data.gt_sg_settings.therm_expan)],...
            ['Hull Length: ',num2str(data.gt_sg_settings.length)]},...
            'Units','normalized','Position',[0.87 0.505 0.12 0.16]);
    end

    function button_regress_plot_velocities(~,~)
        for gstep = 1:numel(parameters)
            settings.(parameters{gstep}) = str2double(get(h_regress_para.(parameters{gstep}).initial,'String'));
        end
        settings.length = data.gt_sg_settings.length;
        run_flightmodel(settings)
        
        glide_vert_spd = [data.flight(dives).glide_vert_spd];
        model_vert_spd = [data.flight(dives).model_vert_spd];
        cast_sign = sign([cast_dir{dives}]);
        depth = [data.hydrography(dives).depth];
        ind = [FRS.use_regress{dives}];
        
        mean_w_H2O = gt_sg_griddata(...
            cast_sign(ind),depth(ind),... % (x and y positions)
            glide_vert_spd(ind) - model_vert_spd(ind),... ( = w_H20 )
            [-1 1],[0:4:1000],@median); % (x and y bins)
        mean_glide = gt_sg_griddata(...
            cast_sign(ind),depth(ind),... % (x and y positions)
            glide_vert_spd(ind),...
            [-1 1],[0:4:1000],@median); % (x and y bins)
        mean_model = gt_sg_griddata(...
            cast_sign(ind),depth(ind),... % (x and y positions)
            model_vert_spd(ind),...
            [-1 1],[0:4:1000],@median); % (x and y bins)
        
        % Plot prelim state for future comparison
        % Switch previous to gray, and delete even older one
        persistent h_regress_vis_glide_old h_regress_vis_model_old h_regress_vis_wh2o_old h_regress_vis_zero_old...
            h_regress_vis_glide h_regress_vis_model h_regress_vis_wh2o h_regress_vis_zero
        try
            delete(h_regress_vis_glide_old);
            delete(h_regress_vis_model_old);
            delete(h_regress_vis_wh2o_old);
            delete(h_regress_vis_zero_old);
            h_regress_vis_glide_old = h_regress_vis_glide;
            h_regress_vis_model_old = h_regress_vis_model;
            h_regress_vis_wh2o_old = h_regress_vis_wh2o;
            h_regress_vis_zero_old = h_regress_vis_zero;
            set(h_regress_vis_glide_old,'Color',[0.8 0.8 0.8],'LineStyle','--');
            set(h_regress_vis_model_old,'Color',[0.8 0.8 0.8],'LineStyle','--');
            set(h_regress_vis_wh2o_old,'Color',[0.8 0.8 0.8],'LineStyle','--');
        end
        h_regress_vis_wh2o = plot(h_regress_vis,[mean_w_H2O(:,1);mean_w_H2O(end:-1:1,2)],[0:4:1000,1000:-4:0],'-g');
        h_regress_vis_glide = plot(h_regress_vis,[mean_glide(:,1);mean_glide(end:-1:1,2)],[0:4:1000,1000:-4:0],'-b');
        h_regress_vis_model = plot(h_regress_vis,[mean_model(:,1);mean_model(end:-1:1,2)],[0:4:1000,1000:-4:0],'-r');
        h_regress_vis_zero = plot(h_regress_vis,[0 0],get(h_regress_vis,'YLim'),'-k');
        axis(h_regress_vis,'ij','tight');
    end

    function score = button_regress_score_minmeanwh2o
        % Score function: root mean square difference of the glide and
        % model vertical speeds. This should effectively minimise w_H20 and
        % hopefully also correct slanted w_H20 linked to thermal expansion
        % in high thermal gradient regions.
        w_H2O = abs([data.flight(dives).glide_vert_spd]-[data.flight(dives).model_vert_spd]);
        score = nansum(w_H2O([FRS.use_regress{dives}])); % TODO BYQ 190726 testing nansum instead of nanmean./ Change back later?
        % TODO: grid in time to avoid aliasaing of vertical sampling of
        % vertically moving features??
    end

    function score = button_regress_score_minmodewh2o
        % Score function: root mean square difference of the glide and
        % model vertical speeds. This should effectively minimise w_H20 and
        % hopefully also correct slanted w_H20 linked to thermal expansion
        % in high thermal gradient regions.
        w_H2O = abs([data.flight(dives).glide_vert_spd]-[data.flight(dives).model_vert_spd]);
        score = mode(round(w_H2O([FRS.use_regress{dives}])*10000000))/10000000;
        % TODO: grid in time to avoid aliasaing of vertical sampling of
        % vertically moving features??
    end

    function score = button_regress_score_minuddiff
        % Score function: root mean square difference of the up and down
        % estimates of vertical velocity across all dives. This requires
        % gridding of all the estimates and then an RMSD.
        % 1. Grid all up/down estimates of w_H20
        glide_vert_spd = [data.flight(dives).glide_vert_spd];
        model_vert_spd = [data.flight(dives).model_vert_spd];
        cast_Dir = [cast_dir{dives}];
        depth = [data.hydrography(dives).depth];
        ind = [FRS.use_regress{dives}];
        w_H2O = gt_sg_griddata(...
            cast_Dir(ind),depth(ind),... % (x and y positions)
            glide_vert_spd(ind) - model_vert_spd(ind),... ( = w_H20 )
            [-max(dives):max(dives)],[0:4:1000],@median); % (x and y bins)
        % w_H2O = downcasts on the left, up casts on the right, with dives
        % mirrored, so "fold" the matrix in the middle and substract the
        % two sides.
        w_H2O = w_H2O(:,max(dives)+1:-1:1) - w_H2O(:,max(dives)+1:end);
        % TODO: Do we want to weight bins by number of samples in each bin
        % (could be useful for severely skewed profile depth distributions)
        score = nanmean(abs(w_H2O(:)));
    end

    function score = button_regress_score_pitchbuoyspace
        % Grid data in buoyancy and pitch space. Return nanmedian of gridded
        % surface, and try to minimise that. Minimising may not be the best
        % but I'm currently struggling to think of computationally cheap
        % way to test if the resulting surface is "flat"... So all around 0
        % will do.
        % Since boyancy is dependent on some regressed parameters, we'll
        % grid in dP/dt pitch space as those are constant.
        % This should provide some benefit as it will provide equal
        % weighting around the parameter space rather than skewing the
        % flight model towards steady sub thermocline conditions where we
        % tend to have much less variance in pitch and buoyancy.
        w_H2O = gt_sg_griddata(...
            [data.flight(dives).glide_vert_spd],... % X
            [data.eng(dives).pitchAng],... % Y
            abs([data.flight(dives).glide_vert_spd]-[data.flight(dives).model_vert_spd]),... % V
            [-1:0.02:1],... % xi
            [-80:80],... % yi
            @mean,... % Median rather than mean.
            0,... % No interpolation
            0); % No plots
        w_Num = gt_sg_griddata(...
            [data.flight(dives).glide_vert_spd],... % X
            [data.eng(dives).pitchAng],... % Y
            abs([data.flight(dives).glide_vert_spd]-[data.flight(dives).model_vert_spd]),... % V
            [-1:0.02:1],... % xi
            [-80:80],... % yi
            @numel,... % Count the points
            0,... % No interpolation
            0); % No plots
        score = nanmean(w_H2O(w_Num > 20));
    end

%% Tab 3 - Visualisation
h_vis_refresh = uicontrol(h_tab(3),'Style','pushbutton','String','Refresh',...
    'Units','normalized','Position',[0.86 0.94 0.13 0.05],...
    'CallBack',@button_vis_refresh);

h_vis_plot.wh2o = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.08 0.57 0.44 .4],'Parent',h_tab(3));
h_vis_plot.wh2odiff = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.08 0.08 0.44 .4],'Parent',h_tab(3));
h_vis_plot.N2 = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.65 0.08 0.34 .85],'Parent',h_tab(3));

vars.abs_salinity = gt_sg_griddata(...
    [cast_dir{dives}],[data.hydrography(dives).depth],... % (x and y positions)
    [data.hydrography(dives).abs_salinity],...
    [-max(dives):max(dives)],[10:4:990],@median); % (x and y bins)
vars.cons_temp = gt_sg_griddata(...
    [cast_dir{dives}],[data.hydrography(dives).depth],... % (x and y positions)
    [data.hydrography(dives).cons_temp],...
    [-max(dives):max(dives)],[10:4:990],@median); % (x and y bins)
vars.pressure = gt_sg_griddata(...
    [cast_dir{dives}],[data.hydrography(dives).depth],... % (x and y positions)
    [data.hydrography(dives).pressure],...
    [-max(dives):max(dives)],[10:4:990],@median); % (x and y bins)

X = [-max(dives):max(dives)];
for kstep=1:numel([-max(dives):max(dives)])
    try
        [vars.N2(:,kstep),~] = gsw_Nsquared(...
            vars.abs_salinity(:,kstep),...
            vars.cons_temp(:,kstep),...
            vars.pressure(:,kstep),...
            data.gps_postdive(abs(X(kstep)),1));
    end
end

    function button_vis_refresh(~,~)
        FRS.dives = gt_sg_sub_check_dive_validity(data);
        gt_sg_sub_echo({'Applying hydrodynamic calculations to all dives.'});
        run_flightmodel(data.gt_sg_settings);
        % Plot difference of up and down casts
        % 1. Grid all up/down estimates of w_H20
        w_H2O = gt_sg_griddata(...
            abs([cast_dir{dives}]),[data.hydrography(dives).depth],... % (x and y positions)
            [data.flight(dives).glide_vert_spd] - [data.flight(dives).model_vert_spd],... ( = w_H20 )
            [1:max(dives)],[10:4:990],@median); % (x and y bins)
        w_H2O_percast = gt_sg_griddata(...
            [cast_dir{dives}],[data.hydrography(dives).depth],... % (x and y positions)
            [data.flight(dives).glide_vert_spd] - [data.flight(dives).model_vert_spd],... ( = w_H20 )
            [-max(dives):max(dives)],[10:4:990],@median); % (x and y bins)
        % w_H2O = downcasts on the left, up casts on the right, with dives
        % mirrored, so "fold" the matrix in the middle and substract the
        % two sides.
        w_H2O_diff = w_H2O_percast(:,max(dives):-1:1) - w_H2O_percast(:,max(dives)+2:end);
        
        h_vis_plot.wh2o_plot = pcolor(h_vis_plot.wh2o,[1:max(dives)],[10:4:990],real(w_H2O));
        set(h_vis_plot.wh2o,'YDir','reverse')
        xlabel(h_vis_plot.wh2o,'Dive Number'); ylabel(h_vis_plot.wh2o,'Depth (m)');
        set(h_vis_plot.wh2o_plot,'EdgeColor','none')
        h_vis_plot.wh2o_cbar = colorbar('peer',h_vis_plot.wh2o); ylabel(h_vis_plot.wh2o_cbar,{'Estimated W_H_2_O'});
        caxis(h_vis_plot.wh2o,[-1 1]*0.02);
        
        h_vis_plot.wh2odiff_plot = pcolor(h_vis_plot.wh2odiff,[1:max(dives)],[10:4:990],real(w_H2O_diff));
        set(h_vis_plot.wh2odiff,'YDir','reverse')
        xlabel(h_vis_plot.wh2odiff,'Dive Number'); ylabel(h_vis_plot.wh2odiff,'Depth (m)');
        set(h_vis_plot.wh2odiff_plot,'EdgeColor','none')
        h_vis_plot.wh2odiff_cbar = colorbar('peer',h_vis_plot.wh2odiff); ylabel(h_vis_plot.wh2odiff_cbar,{'Absolute difference between up and downcast estimates of W_H_2_O','Ideally, it should appear as random noise dissimilar T&S structures or dive profile shape.'});
        caxis(h_vis_plot.wh2odiff,[-0.02 0.02]);
        
        w_H2O_N2 = w_H2O_percast(1:end-1,:) + diff(w_H2O_percast);
        h_vis_plot.N2_plot = plot(h_vis_plot.N2,...
            vars.N2(:),...
            w_H2O_N2(:).^2,...
            '-k','Marker','.','LineStyle','none');
        xlabel(h_vis_plot.N2,'N^2 (s^-^2)'); ylabel(h_vis_plot.N2,'W_H_2_O^2 (cm^2 s^-^2)');
        set(h_vis_plot.N2,'XLim',[0 5e-4],'YLim',[0 0.003]);
    end

% W vs Temp, Depth, Time
% Up vs Down
% Histograms like in Frajka Williams

%% Tab 4 - History
h_param_save = uicontrol(h_tab(4),'Style','pushbutton','String','Save notes',...
    'Units','normalized','Position',[0.86 0.94 0.13 0.05],...
    'CallBack',@button_his_save);

columnname = {'Date','hd_a','hd_b','hd_c','hd_s','volmax','abs_compress','therm_expan','Notes'};
columnformat = {'char','char','char','char','char','char','char','char','char'};

if ~isfield(data.gt_sg_settings,'regress_history')
    data.gt_sg_settings.regress_history = {datestr(now),data.gt_sg_settings.hd_a,data.gt_sg_settings.hd_b,data.gt_sg_settings.hd_c,data.gt_sg_settings.hd_s,data.gt_sg_settings.volmax,data.gt_sg_settings.abs_compress,data.gt_sg_settings.therm_expan,' '};
end

h_his_tables = uitable(h_tab(4),'Data', data.gt_sg_settings.regress_history,...
    'ColumnName', columnname,...
    'ColumnFormat', columnformat,...
    'ColumnEditable', [false false false false false false false false true],...
    'ColumnWidth',{200 150 150 150 150 150 150 150 200},...
    'RowName',[],...
    'Units','normalized','Position',[0.05 0.05 0.9 0.85],...
    'CellEditCallback',@button_his_save);

    function button_his_save(~,~)
        data.gt_sg_settings.regress_history = get(h_his_tables,'Data');
    end

%% Tab 5 - HEEEEEEEEELP ME !
h_auto_process = uicontrol(h_tab(5),'Style','pushbutton','String','Automatic regression',...
    'Units','normalized','Position',[0.86 0.94 0.13 0.05],...
    'CallBack',@button_regress_auto);

uicontrol('Parent',h_tab(5),'Style','text','String',HelpInfo,...
    'Units','normalized','Position',[0.05 0.05 0.5 0.9])

%% Subfunctions
    function out = indicesIn(list,sublist)
        [~, out, ~] = intersect(list,sublist);
    end

% Create get_exp handle to make all regressed coefficients of similar
% units (works better).
get_exp = @(x) real(floor(log10(x)));
get_coeff = @(x) x.*10.^-get_exp(x);

    function calcCurrents
        % Calculate vertical velocities, depth-averaged currents and interpolated GPS positions
        if isfield(data.flight,'model_spd')
            gt_sg_sub_echo({'Estimating up/downwelling, depth-average currents and underwater coordinates.'});
            data = gt_sg_sub_currents(data);
        else
            gt_sg_sub_echo({'No flight model output available.',...
                'Cannot estimate up/downwelling, depth-averaged currents or',...
                'coordinates of the glider underwater.',...
                'WARNING: Flight model data should be present, something has gone wrong'});
        end
    end

%% Flight model scripts
%%%%%%%%%%%%%% START OF FLIGHT MODEL ROUTINES %%%%%%%%%%%%%%
    function run_flightmodel(settings)
        for istep = FRS.dives
            % settings. should have volmax,hd_a,hd_b,hd_c,hd_s,abs_compress,therm_expan,length
            % Determine glider buoyancy based on volmax and in situ density
            % Calculate volume
            vbd = data.eng(istep).vbdCC; % cm^3
            vol0 = settings.volmax + (data.log(istep).C_VBD - data.log(istep).VBD_MIN)/data.gt_sg_settings.vbd_cnts_per_cc; % cm^3
            vol = (vol0 + vbd) .* ...
                exp(-settings.abs_compress * data.hydrography(istep).pressure + settings.therm_expan * (data.hydrography(istep).temp - data.gt_sg_settings.temp_ref)); % cm^3
            
            % Calculate buoyancy of the glider
            % Buoyancy units
            %   kg + (kg.m-3 * cm^3 * 10^-6)
            % = kg + (kg.m-3 * m^3)
            % = kg
            data.flight(istep).buoyancy =  -data.gt_sg_settings.mass + ( data.hydrography(istep).rho .* vol * 1.e-6 ); % kg
            
            % Calculate glide slope and speed
            [data.flight(istep).model_spd, data.flight(istep).model_slope]... % m.s-1, degrees
                = flightvec(...
                data.flight(istep).buoyancy,... % kg
                data.eng(istep).pitchAng,... % degrees
                settings.length,... % Hull length in meters, excluding antenna
                settings.hd_a,... % rad^-1
                settings.hd_b,... % m^1/4 . kg^1/4 . s^-1/2
                settings.hd_c,... % rad^-2
                settings.hd_s,... % how the drag scales by shape
                istep); % kg.m-3
            
            % Determine vertical and horizontal speed
            data.flight(istep).model_horz_spd = data.flight(istep).model_spd.*cosd(data.flight(istep).model_slope);
            data.flight(istep).model_vert_spd = data.flight(istep).model_spd.*sind(data.flight(istep).model_slope);
        end
    end

    function run_flightmodel_varHydro(settings)
        for istep = FRS.dives
            % settings. should have volmax,hd_a,hd_b,hd_c,hd_s,abs_compress,therm_expan,length
            % Determine glider buoyancy based on volmax and in situ density
            % Calculate volume
            vbd = data.eng(istep).vbdCC; % cm^3
            vol0 = settings.volmax(istep) + (data.log(istep).C_VBD - data.gt_sg_settings.vbd_min_cnts)/data.gt_sg_settings.vbd_cnts_per_cc; % cm^3
            vol = (vol0 + vbd) .* ...
                exp(-settings.abs_compress(istep) * data.hydrography(istep).pressure + settings.therm_expan(istep) * (data.hydrography(istep).temp - data.gt_sg_settings.temp_ref)); % cm^3
            
            % Calculate buoyancy of the glider
            % Buoyancy units
            %   kg + (kg.m-3 * cm^3 * 10^-6)
            % = kg + (kg.m-3 * m^3)
            % = kg
            data.flight(istep).buoyancy =  -data.gt_sg_settings.mass + ( data.hydrography(istep).rho .* vol * 1.e-6 ); % kg
            
            % Calculate glide slope and speed
            [data.flight(istep).model_spd, data.flight(istep).model_slope]... % m.s-1, degrees
                = flightvec(...
                data.flight(istep).buoyancy,... % kg
                data.eng(istep).pitchAng,... % degrees
                settings.length,... % Hull length in meters, excluding antenna
                settings.hd_a(istep),... % rad^-1
                settings.hd_b(istep),... % m^1/4 . kg^1/4 . s^-1/2
                settings.hd_c(istep),... % rad^-2
                settings.hd_s(istep),... % how the drag scales by shape
                istep); % kg.m-3
            
            % Determine vertical and horizontal speed
            data.flight(istep).model_horz_spd = data.flight(istep).model_spd.*cosd(data.flight(istep).model_slope);
            data.flight(istep).model_vert_spd = data.flight(istep).model_spd.*sind(data.flight(istep).model_slope);
        end
    end

    function [ u_mag, theta ] = flightvec(buoyancy_v, vehicle_pitch_degrees_v, glider_length, hd_a, hd_b, hd_c, hd_s, istep)
        %         Compute vehicle speed and glide angle from buoyancy and observed pitch
        %
        %         Usage: [speed glide_angle] = hydro_model(buoyancy_v, vehicle_pitch_degrees_v, calib_consts)
        %         where [speed glide_angle] is a 2 x n_pts matrix
        %
        %         Input:
        %         buoyancy_v - n_pts vector (grams, positive is upward)
        %         vehicle_pitch_degrees_v - observed vehicle pitch (degrees (! not radians), positive nose up)
        %         calib_consts - that contain
        %         hd_a, hd_b, hd_c, hd_s - hydrodynamic parameters for
        %         lift, drag, induced drag, and magic
        %         rho - density of deep water (maximum density encountered)
        %         glider_length - the length of the vehicle
        %
        %         Output:
        %         converged - whether the iterative solution converged
        %         theta - glide angle in radians, positive nose up
        %         umag - total vehicle speed through the water (cm/s)
        %         stalled_i_v - locations where stalled
        %
        %         Reference:
        %         flightvec0.m (CCE)
        %         Eriksen, C. C., et al: IEEE Journal of Oceanic Engineering, v26, no.4, October, 2001.
        %
        %         hd_a is in 1/deg. units, hd_b has dimensions q^(1/4), hd_c is in 1/deg.^2 units
        gravity = 9.82; % m/s2
        m2cm = 100; % m to cm
        cm2m = 0.01; % cm to m
        
        % Size of the vectors
        num_rows = numel(buoyancy_v);
        
        % Compute constants used in hydrodynamic computations (for efficiency)
        % These are used to compute the (inverted) performance factor (param) under the sqrt in Eqn 8
        l2 = glider_length .* glider_length;
        l2_hd_b2 = 2 .* l2 .* hd_b;
        hd_a2 = hd_a .* hd_a;
        hd_bc4 = 4 .* hd_b .* hd_c;
        hd_c2 = 2 .* hd_c;
        
        buoyancy_sign_v = sign(buoyancy_v); % never non-zero
        pitched_i_v = vehicle_pitch_degrees_v  ~= 0;
        pitch_sign_v = ones(1, num_rows); % if flat, assume sign is 1.0
        pitch_sign_v(pitched_i_v) = sign(vehicle_pitch_degrees_v(pitched_i_v));
        % Compute points where flight is expected: where buoyancy and pitch are both up or both down
        buoyancy_pitch_ok_v =  buoyancy_sign_v .* pitch_sign_v > 0.0;
        
        % compute buoyancy force from buoyancy F = ma = (g)*(g2kg)*(m/s)^2 [Newtons]
        buoyancy_force_v = buoyancy_v .* gravity; % BYQ: removed g2kg bit as already in kg when produced by toolbox
        
        % Initially assume the glide angle (theta) is +/-pi/4 radians (45 degrees) (climb or dive, respectively)
        % We iterate below to determine the actual glide slope
        theta = (pi/4.0) .* buoyancy_sign_v;
        
        % Compute initial dynamic pressure q for vertical flight
        % Initial q is determined from the drag eqn. (Eqn. 2 in Eriksen et al.) by assuming attack angle alpha = 0
        % and that we are completely vertical (sin(90) = 1) so all drag, no lift
        % This always a positive quantity thanks to the buoyancy_sign_v value above
        % We deliberately start far away from the soln so both q and th, set independently here,
        % are able to relax together to a consistent soln.
        
        % BUG: Depending on initial abc values this formulation of 'q_old' will be arbitrarily close to q below
        % and terminate the loop after a single iteration with poor th (and q).
        % If the loop is forced to run at least twice, both q and th are updated and we make proper progress toward the soln.
        % In particular, this problematic initial q expression from flightvec2 triggers the problem with more regularity.
        % DEAD q = power((buoyancy_force_v*sin(theta))/(l2*hd_b), 1/(1+hd_s))
        
        % This original version, without the sin(th) term, ensures q and q_old are sufficiently different
        % that we don't stall out the loop; nevertheless we ensure two loops below to get q close first, then th
        q = power(buoyancy_sign_v .* buoyancy_force_v ./ (l2.*hd_b),...
            1 ./ (1+hd_s)); % dynamic pressure for vertical flight
        
        % This loop iterates the dynamic pressure (q) to get glidespeed
        % and the attack angle (alpha) to get glideangle (theta)
        % buoyancy is taken to be an accurately known quantity and is not iterated here (but see caller)
        % We iterate a fixed number of times but break early if our max % difference is less that residual_test
        residual_test = 0.001 ;  % loop completion test value
        for loop_count = 1:15 % flightvec0 goes from 0 to 15
            q_prev = q; % copy q
            
            % discriminant_inv is the reciprocal of the term subtracted
            % from 1 under the radical in the solution equations (Eqs. 7-8)
            % (lambda*tan(theta)^2/4)
            % discriminant_inv is estimated using the current theta
            scaled_drag = power(q_prev, -hd_s); % invert the sign here because we'll use it inverted below
            discriminant_inv_v = hd_a2 .* tan(theta) .* tan(theta) .* scaled_drag ./ hd_bc4;
            % NOTE: Beware when comparing this version with flightvec(2).m
            % In those version, when stalled the entries are replaced by nan rather than 0
            % Thus in matlab you need to use nanmean, etc. and here you have to be careful because of all the zeros dragging the mean down
            
            % valid solutions exist only for discriminant_inv > 1 (zero or complex otherwise)
            % also only consider points where flight is valid
            flying_i = discriminant_inv_v > 1.0 & buoyancy_pitch_ok_v;
            q(:) = 0; % assume the worst..stalled, no dynamic pressure or glide angle
            
            if isempty(flying_i)
                % Probably should declare the points bad or skip this profile (suggested elsewhere)
                % but in case this hasn't happened handle things gracefully without throwing exceptions.
                gt_sg_sub_echo({['No valid flight points on dive ',num2str(FRS.dives(istep))],...
                    'Cannot calculate flight model'});
                % Report unable to converge and mark all points as stalled
                % to forestall further processing by caller
                % return umag as zero since q is zero
                u_mag = nan(size(buoyancy_v));
                theta = nan(size(buoyancy_v));
                return
            end
            
            sqrt_discriminant = sqrt(1 - 1 ./ discriminant_inv_v(flying_i));
            % Eq. 7 in the reference, obtained using the quadratic formula ...
            % q^(hd_s) considered to vary slowly compared to q (hd_s typically <= -1/4)
            % NOTE the q eqn use 1.0 + sqrt and the alpha eqn must use 1.0 - sqrt
            q(flying_i) = (buoyancy_force_v(flying_i) .* sin(theta(flying_i)) .* scaled_drag(flying_i)) ...
                ./ (l2_hd_b2) .* (1.0 + sqrt_discriminant);
            % Eq. 8 in the reference, with critical minus sign
            % hd_a is 1/deg; hd_c is 1/deg^2 so overall alpha is in degrees
            alpha = (-hd_a .* tan(theta(flying_i)) ./ hd_c2) .* (1.0 - sqrt_discriminant); % degrees  %% TODO: BYQ Is it really?
            theta(:) = 0; % assume the worst..stalled % == math.radians(0.0)
            % defn: pitch  = glide + attack angles
            % glideangle is the difference between pitch and angle of attack (both in degrees)
            theta(flying_i) = deg2rad(vehicle_pitch_degrees_v(flying_i) - alpha);
            max_residual = max(abs((q(flying_i) - q_prev(flying_i))/q(flying_i)));
            
            if max_residual < residual_test && loop_count >= 3 % ensure at least 2 iterations
                break   % break out of loop_count loop
            end
        end
        % end of loop_count loop
        
        % compute total estimated speed through the water (rename from u_mag (which is confusing with U in the paper) to total_speed_cm_s)
        % defn: q = rho0/2*(U^2 + W^2) = rho0/2*total_speed^2
        u_mag = sqrt(2.0 .*q ./ data.hydrography(istep).rho); %% TODO: BYQ Should this be actual rho from .hydrography? /// TESTING, used to be rho0 from sg_cal_const
        
        % NOTE flightvec_new.m and glide.m have code that looks for places where the final calc indicates a stall (theta  == 0)
        % and linearly interpolates a glide angle from the surrounding points (see sophisticated interpotation in flightvec_new.m)
        % and then interpolates q similarly (but could be at different points) after which u_mag would be be calculated
        
        % Determine other stalls...
        stalled_i_v = u_mag <= 0; % TODO: Add more parameters here
        
        u_mag(stalled_i_v | ~buoyancy_pitch_ok_v) = 0;
        theta(stalled_i_v | ~buoyancy_pitch_ok_v) = 0;
        
        theta = rad2deg(theta);
    end
%%%%%%%%%%%%%% END OF FLIGHT MODEL ROUTINES %%%%%%%%%%%%%%

%% WAIT FOR GUI CLOSE IF RUNNING INTERACTIVE, OTHERWISE RUN AUTO FUNCTION AND QUIT

if timevar
    for pstep = 1:numel(parameters)
        data.gt_sg_settings.(parameters{pstep}) = data.gt_sg_settings.(parameters{pstep})(1);
        set(h_regress_para.(parameters{pstep}).initial,'String',data.gt_sg_settings.(parameters{pstep}));
    end

    chunk_size = 30;
    
    gt_sg_sub_echo({'Calculating hd_a, hd_b, hd_s and volmax in 30 dive chunks.','Waring: this is a lengthy process.'});
    
    num_passes = ceil(numel(dives)/chunk_size);
    
    tvar.hd_a(1:num_passes) = nan;
    tvar.hd_b = tvar.hd_a; tvar.hd_s = tvar.hd_a; tvar.volmax = tvar.hd_a;
    tvar.date = tvar.hd_a;
    
    h_waitbar = waitbar(0,'Processing hydrodynamic coefficients...');
    for rstep = 1:num_passes
        start_dive = chunk_size*(rstep-1);
        FRS.dives = intersect(start_dive+[1:chunk_size],dives);
        
        button_regress_auto(0,0);
        
        tvar.hd_a(rstep) = data.gt_sg_settings.hd_a
        tvar.hd_b(rstep) = data.gt_sg_settings.hd_b
        tvar.hd_s(rstep) = data.gt_sg_settings.hd_s
        tvar.volmax(rstep) = data.gt_sg_settings.volmax
        tvar.date(rstep) = nanmean(data.date(FRS.dives))
        datestr(tvar.date(rstep))
        
        waitbar(rstep./num_passes,h_waitbar);
    end
    
    data.gt_sg_settings.hd_a = polyval(polyfit(tvar.date,tvar.hd_a,2),data.date);
    data.gt_sg_settings.hd_b = polyval(polyfit(tvar.date,tvar.hd_b,2),data.date);
    data.gt_sg_settings.hd_s = polyval(polyfit(tvar.date,tvar.hd_s,2),data.date);
    data.gt_sg_settings.volmax = polyval(polyfit(tvar.date,tvar.volmax,2),data.date);
    close(h_waitbar);
end

if any([numel(data.gt_sg_settings.hd_a),...
        numel(data.gt_sg_settings.hd_b),...
        numel(data.gt_sg_settings.hd_c),...
        numel(data.gt_sg_settings.hd_s),...
        numel(data.gt_sg_settings.volmax),...
        numel(data.gt_sg_settings.therm_expan),...
        numel(data.gt_sg_settings.abs_compress)] == numel(data.hydrography))
    
    gt_sg_sub_echo({'Applying variable hydrodynamic parameters define in gt_sg_settings.','Bypassing regression GUI and proceeding straight to calculations.'});
    
    scalar_fields = {};
    fields = {'hd_a','hd_b','hd_c','hd_s',...
        'volmax','abs_compress','therm_expan'};
    
    % Make all the same size
    for fieldstep = 1:numel(fields)
        field = fields{fieldstep};
        if isscalar(data.gt_sg_settings.(field))
            data.gt_sg_settings.(field) = ones(size(data.hydrography)) .* data.gt_sg_settings.(field);    
            scalar_fields{end+1} = field;
        end
    end
    
    % Run processing
    run_flightmodel_varHydro(data.gt_sg_settings);
    
%     % Return to initial size
%     for fieldstep = 1:numel(scalar_fields)
%         field = scalar_fields{fieldstep};
%         data.gt_sg_settings.(field) = mode(data.gt_sg_settings.(field));
%     end
    
    calcCurrents;
    return;
end

if auto
    button_regress_auto(0,0);
    FRS.dives = gt_sg_sub_check_dive_validity(data);
    run_flightmodel(data.gt_sg_settings);
    close(h_gui);
    gt_sg_sub_echo(['Exporting new flight coefficients to gt_sg_settings.']);
    gt_sg_sub_echo({['To save time in future, we suggest adding the following to sg_calib_constants.m'],...
        ['  hd_a = ',num2str(data.gt_sg_settings.hd_a,9),';'],...
        ['  hd_b = ',num2str(data.gt_sg_settings.hd_b,9),';'],...
        ['  hd_c = ',num2str(data.gt_sg_settings.hd_c,9),';'],...
        ['  hd_s = ',num2str(data.gt_sg_settings.hd_s,9),';'],...
        ['  volmax = ',num2str(data.gt_sg_settings.volmax,9),';'],...
        ['  abs_compress = ',num2str(data.gt_sg_settings.abs_compress,9),';'],...
        ['  therm_expan = ',num2str(data.gt_sg_settings.therm_expan,9),';']...
        });
    calcCurrents;
    return;
end

run_flightmodel(data.gt_sg_settings);
uiwait(h_gui)
FRS.dives = gt_sg_sub_check_dive_validity(data);
gt_sg_sub_echo({'Applying hydrodynamic calculations to all dives.'});
run_flightmodel(data.gt_sg_settings);

gt_sg_sub_echo(['Exporting new flight coefficients to gt_sg_settings.']);
gt_sg_sub_echo({['To save time in future, we suggest adding the following to sg_calib_constants.m'],...
    ['  hd_a = ',num2str(data.gt_sg_settings.hd_a,9),';'],...
    ['  hd_b = ',num2str(data.gt_sg_settings.hd_b,9),';'],...
    ['  hd_c = ',num2str(data.gt_sg_settings.hd_c,9),';'],...
    ['  hd_s = ',num2str(data.gt_sg_settings.hd_s,9),';'],...
    ['  volmax = ',num2str(data.gt_sg_settings.volmax,9),';'],...
    ['  abs_compress = ',num2str(data.gt_sg_settings.abs_compress,9),';'],...
    ['  therm_expan = ',num2str(data.gt_sg_settings.therm_expan,9),';']...
    });
calcCurrents;

end
