function dx = gt_sg_sub_diff(x,ignoreNaNs)
%
% dx = gt_sg_sub_diff(x)
% First order differentiation returning an array of the same length.
% In an array is input, the it will return dx, if a cell array containing
% two same length arrays is input, it will return dx{1}/dx{2}.
%
% Inputs:
% x = array or 2x1 cell array containing two arrays of the same length.
%
% Outputs:
% dx = first order differential of x
%
% B.Y.QUESTE May 2019
%
if nargin < 2
    ignoreNaNs = true;
end

doDiff = @(in) nanmean([[nan;diff(in(:))],[diff(in(:));nan]],2);

if iscell(x)
    
    in1 = x{1};
    in2 = x{2};
    
    if ignoreNaNs == 1
        NotNaNs = find(~isnan(in1+in2));
    else
        NotNaNs = 1:numel(in1);
    end
    
    % TODO: This is Sunke's method. Verify it makes sense. Produces
    % different results to diff etc (slightly smoothed). Is this an issue?
    d1 = doDiff(in1(NotNaNs));
    
    d2 = doDiff(in2(NotNaNs));
    
    dx = nan(size(in1));
    dx(NotNaNs) = d1./d2;

else
    
    if ignoreNaNs == 1
        NotNaNs = find(~isnan(x));
    else
        NotNaNs = 1:numel(x);
    end
    
    d1 = doDiff(x(NotNaNs));
    
    dx = nan(size(x));
    dx(NotNaNs) = d1;
end
end